const mongoose = require('mongoose');

const { ObjectId } = mongoose.Types;

// Aqui havia um erro difícil de pegar. Importei como "transactionModel",
// com "t" minúsculo. No Windows, isso não faz diferença. Mas como no Heroku
// o servidor é Linux, isso faz diferença. Gastei umas boas horas tentando
// descobrir esse erro :-/
const TransactionModel = require('../models/TransactionModel');

function extractCleanTransactionFrom(mongoDbTransaction) {
  const {
    _id,
    description,
    value,
    category,
    year,
    month,
    day,
    yearMonth,
    yearMonthDay,
    type,
  } = mongoDbTransaction;

  const returnedTransaction = {
    _id,
    description,
    value,
    category,
    year,
    month,
    day,
    yearMonth,
    yearMonthDay,
    type,
  };

  return returnedTransaction;
}

async function getTransactionFrom(period) {
  const transaction = await TransactionModel.find({ yearMonth: period });
  return transaction;
}

async function postTransaction(transaction) {
  const newTransactionMongoDB = await TransactionModel.create(transaction);
  const newTransaction = extractCleanTransactionFrom(newTransactionMongoDB);
  return newTransaction;
}

async function updateTransaction(_id, transaction) {
  await TransactionModel.updateOne({ _id: Object(_id) }, transaction);
  return { _id, ...transaction };
}

async function deleteTransaction(_id) {
  await TransactionModel.deleteOne({ _id: ObjectId(_id) });
}

module.exports = {
  getTransactionFrom,
  postTransaction,
  updateTransaction,
  deleteTransaction,
};
