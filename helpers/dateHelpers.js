function validatePeriod(period) {
  if (period.length !== 7) {
    throw new Error(`Valor inválido (${period}). Utilize o formato yyyy-mm`);
  }

  const year = period.substring(0, 4);
  if (!+year) {
    throw new Error(`Ano inválido (${year})`);
  }

  const month = period.substring(5, 7);
  if (!+month || +month < 1 || +month > 12) {
    throw new Error(`Mês inválido (${month})`);
  }
}

function createPeriodFrom(year, month) {
  return `${year.toString().padStart(4, '0')}-${month
    .toString()
    .padStart(2, '0')}}`;
}

function createDateFrom(year, month, day) {
  return `${createPeriodFrom(year, month)}-${day.toString().padStart(2, '0')}`;
}


